import fs from "fs/promises";
import { Cluster } from "puppeteer-cluster";

const readFile = async (filePath: string) => fs.readFile(filePath, "utf8");

const WIDTH = 1920 / 3;
const HEIGHT = 1080 / 3;

(async () => {
  const locations = await readFile("locations.txt").then((x) => x.split("\n"));
  const locs = locations;

  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    maxConcurrency: 12,
    monitor: true,
  });

  const combinations: [string, number][] = [];
  locs.forEach((viewpoint) => {
    [0, 120, 240].forEach((heading) => {
      combinations.push([viewpoint, heading]);
    });
  });

  await cluster.task(async ({ page, data: { viewpoint, heading } }) => {
    await page.setViewport({ height: HEIGHT, width: WIDTH });
    const url = `https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=${viewpoint}&heading=${heading}&pitch=0&fov=120`;
    await page.goto(url);
    await page.waitForNavigation();
    await page.waitForNetworkIdle({ idleTime: 10 });
    return await page.screenshot({
      path: `images/${viewpoint}__${heading}.png`,
    });
  });

  combinations.map(([viewpoint, heading]) => {
    cluster.queue({ viewpoint, heading });
  });

  await cluster.idle();
  await cluster.close();
})();
