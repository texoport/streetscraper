import path from "path";
import { Page } from "puppeteer";

const WIDTH = 1920 / 2;
const HEIGHT = 1080 / 2;

export const getPanorama = async (
  page: Page,
  viewpoint: string,
  heading: number
) => {
  await page.setViewport({ height: HEIGHT, width: WIDTH });
  const url = `https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=${viewpoint}&heading=${heading}&pitch=0&fov=120`;
  await page.goto(url);
  await page.waitForNavigation();
  await page.waitForNetworkIdle({ idleTime: 1000 });
  console.log(`images/${viewpoint}__${heading}.png`);
  return await page.screenshot({
    path: `images/${viewpoint}__${heading}.png`,
  });
};

export const getLocations = async (page: Page, n: number) => {
  await page.setCacheEnabled(false);
  await page.goto(`file:${path.join(__dirname, "index.html")}`);
  const locations: string[] = [];

  page.evaluate((n) => {
    const streetViewService = new google.maps.StreetViewService();
    const arr = new Array(n).fill(0);
    arr.forEach(async () => {
      const lng = Math.random() * 360 - 180;
      const lat = Math.random() * 180 - 90;

      await new Promise((resolve) => {
        streetViewService.getPanorama(
          {
            location: { lat, lng },
            preference: google.maps.StreetViewPreference.BEST,
            radius: 500000,
            source: google.maps.StreetViewSource.OUTDOOR,
          },
          async (result, status) => {
            if (status !== "ZERO_RESULTS") {
              const location = result?.location?.latLng;
              if (location) {
                // Very important lol
                console.log(`${location.lat()},${location.lng()}`);
              }
              return resolve(result);
            }
          }
        );
      });
    });
  }, n);

  page.on("console", (message) => {
    const text = message.text();
    // hacky AF lul
    if (text[0] !== "G") locations.push(text);
  });

  await page.waitForNetworkIdle();
  return locations;
};
