import fs from "fs/promises";
import { getLocations } from "lib";
import { Cluster } from "puppeteer-cluster";

// (async () => {
// const browser = await puppeteer.launch({
//   headless: true,
//   args: ["--no-sandbox", "--disable-setuid-sandbox", "--mute-audio"],
// });
// const locations = await getLocations(browser, N);
// console.log(`${locations.length} locations obtained`);
// const combinations: [string, number][] = [];
// locations.forEach((l) => {
//   [0, 120, 240].forEach((heading) => {
//     combinations.push([l, heading]);
//   });
// });
// for (let i = 0; i < combinations.length; i += BATCH) {
//   await getPanorama(browser, combinations[i][0], combinations[i][1]);
//   await Promise.all(
//     combinations
//       .slice(i, i + BATCH)
//       .map(([location, heading]) => getPanorama(browser, location, heading))
//   );
// }
// browser.close();
// })();

(async () => {
  const cluster = await Cluster.launch({
    concurrency: Cluster.CONCURRENCY_CONTEXT,
    maxConcurrency: 4,
  });

  await cluster.task(async ({ page }) => {
    const locations = await getLocations(page, 1000);
    await fs.appendFile("locations.txt", locations.join("\n"));
    await fs.appendFile("locations.txt", "\n");
    console.log("done");
  });

  await cluster.queue(null);
  await cluster.queue(null);
  await cluster.queue(null);

  await cluster.idle();
  await cluster.close();
})();
